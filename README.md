## Práctica

1) Dado un string, comprobar si es palíndromo.
  Por ejemplo:
    * Para `inputString = "aabaa"` el resultado debería ser `palindrome? (inputString) = true`
    * Para `inputString = "abac"`, el resultado debería ser `palindrome? (inputString) = false`
    * Para `inputString = "a"`, el resultado debería ser `palindrome? (inputString) = true`

2) Dado un arreglo de enteros, se quiere ordenar de menor a mayor en donde cada valor sea más grande que el anterior exactamente en 1. Retornar la CANTIDAD MINIMA de numeros adicionales necesarios.
  Por ejemplo:
    * Para `array = [6, 2, 3, 8]`, el resultado debe ser `array_consecutive(array) = 3`. Ya que se necesitan los valores  `4`, `5` y `7`.

3) Dado un arreglo de strings, retornar otro el cual contenga TODOS los strings mas largos.
  Por ejmplo:
    * Para `inputArray = ["aba", "aa", "ad", "vcd", "aba"]`, se espera la salida `all_longest_strings(inputArray) = ["aba", "vcd", "aba"]`

<!--v-->
<!-- .slide: data-background="./assets/snappler/main_background.png" -->

## Extra

4) Dado el siguiente archivo con altitudes en metros, se quiere averiguar que tan rápido crecen.

`measurements.txt`
```
199
200
208
210
200
207
240
```

Para hacer esto, cuente el número de veces que una medición aumenta con respecto a la anterior. (No hay ninguna medición antes de la primera). En el ejemplo anterior, los cambios son los siguientes:

```
199 (N/A - no previous measurement)
200 (increased)
208 (increased)
210 (increased)
200 (decreased)
207 (increased)
240 (increased)
```

En este ejemplo, el valor a retornar seria **5** ya que es la cantidad de medidas que son más grandes que la anterior.

5) Sea el siguiente dump de base de datos:

`people.csv`

|id|name|email|birth_date|created_at|updated_at| 
| -- | -- | -- | -- | -- | -- |
|1|Burks, Rosella|burks@example.com|2006-07-15|2016-07-15 18:16:13|2016-07-15 18:16:13| 
|2|Avila, Damien|avila@example.com|2001-12-15|2016-07-15 18:16:13|2016-07-15 18:16:13| 
|3|Olsen, Robin|olsen@example.com|1998-04-25|2016-07-15 18:16:13|2016-07-15 18:16:13| 
|4|Moises, Edgar|moises@example.com|2004-02-28|2016-07-15 18:16:13|2016-07-15 18:16:13| 
|5|Claude, Elvin|claude@example.com|1996-07-31|2016-07-15 18:16:13|2016-07-15 18:16:13| 

  * Imprimir los mails de las personas mayores a 18 años.
